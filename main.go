package main

import (
	"github.com/gin-gonic/gin"
)

func healthz(c *gin.Context) {
	c.String(200, "ok")
}

func setupRouter() *gin.Engine {
	gin.DisableConsoleColor()
	r := gin.Default()
	r.GET("/healthz", healthz)
	return r
}

func main() {
	r := setupRouter()
	r.Run(":8087")
}
