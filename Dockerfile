# Build stage
FROM golang:1.10-alpine AS build-env
MAINTAINER Kyle Bai <k2r2.bai@gmail.com>

ENV GOPATH "/go"
ADD ./ /go/src/github.com/kairen/push-harbor
RUN apk add --no-cache git && \
  go get -u github.com/golang/dep/cmd/dep && \
  cd /go/src/github.com/kairen/push-harbor && \
  dep ensure && go build -o ph

# Run stage
FROM alpine:3.7
COPY --from=build-env /go/src/github.com/kairen/push-harbor/ph /bin/ph
RUN apk add --no-cache util-linux bash && \
  rm -f /var/cache/apk/*

ENTRYPOINT ["/bin/ph"]
