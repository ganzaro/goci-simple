#!/bin/sh

set -e

apk add --no-cache git && \
  go get -u github.com/golang/dep/cmd/dep && \
  cd /go/src/github.com/kairen/push-harbor && \
  dep ensure && go test
